const express = require("express");
const app = express();

app.use(express.static("www"));

app.listen(3000, () => {
    console.log("Started development server. At http://backend:3000");
})