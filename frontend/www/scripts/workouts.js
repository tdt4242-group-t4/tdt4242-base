let FILTER = {
  searchString: undefined,
  exerciseType: "any",
  workoutVisibility: undefined,
  dateRange: [new Date("2021-01-01T13:00:00"), new Date()],
};

function renderWorkouts(workouts) {
  let container = document.getElementById("div-content");
  workouts.forEach((workout) => {
    let templateWorkout = document.querySelector("#template-workout");
    let cloneWorkout = templateWorkout.content.cloneNode(true);

    let aWorkout = cloneWorkout.querySelector("a");
    aWorkout.href = `workout.html?id=${workout.id}`;
    aWorkout.id = workout.id;

    let h5 = aWorkout.querySelector("h5");
    h5.textContent = workout.name;

    let localDate = new Date(workout.date);

    let table = aWorkout.querySelector("table");
    let rows = table.querySelectorAll("tr");
    rows[0].querySelectorAll(
      "td"
    )[1].textContent = localDate.toLocaleDateString(); // Date
    rows[1].querySelectorAll(
      "td"
    )[1].textContent = localDate.toLocaleTimeString(); // Time
    rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
    rows[3].querySelectorAll("td")[1].textContent =
      workout.exercise_instances.length; // Exercises

    container.appendChild(aWorkout);
  });
}

function renderExerciseOptions(exerciseTypes, favorites) {
  const selectElement = document.querySelector("#exercise-filter-select");
  if (favorites !== undefined) {
    let user_id = JSON.parse(
      atob(
        ("; " + document.cookie)
          .split("; access=")
          .pop()
          .split(";")
          .shift()
          .split(".")[1]
      )
    )["user_id"];
    favorites = favorites.filter(
      (favorite) => parseInt(favorite.user) === parseInt(user_id)
    );
    exerciseTypes = exerciseTypes
      .sort((a, b) => {
        if (
          favorites.some((favorite) => favorite.exercise === a.id) &&
          !favorites.some((favorite) => favorite.exercise === b.id)
        )
          return 1;
        else if (
          !favorites.some((favorite) => favorite.exercise === a.id) &&
          favorites.some((favorite) => favorite.exercise === b.id)
        )
          return -1;
        else return 0;
      })
      .reverse();
  }
  exerciseTypes.forEach((exerciseType) => {
    const option = document.createElement("option");
    option.value = exerciseType.url;
    option.innerText = exerciseType.name;
    selectElement.append(option);
  });
}

function createWorkout() {
  window.location.replace("workout.html");
}

function filterVisibilityType(currentUser, workout, visibilityType) {
  let result;

  switch (visibilityType) {
    case "list-my-workouts-list":
      result = workout.owner == currentUser.url;
      break;
    case "list-athlete-workouts-list":
      result =
        currentUser.athletes && currentUser.athletes.includes(workout.owner);
      break;
    case "list-public-workouts-list":
      result = workout.visibility == "PU";
      break;
    default:
      result = true;
      break;
  }

  return result;
}

function filterByExerciseType(workout, exerciseType) {
  return (
    workout.exercise_instances
      .map((exerciseInstance) => exerciseInstance.exercise)
      .includes(exerciseType) || exerciseType == "any"
  );
}

function filterByDateRange(workout, dateRange) {
  const workoutDate = new Date(workout.date);
  const fromDate = new Date(dateRange[0]);
  const toDate = new Date(dateRange[1]);

  return fromDate <= workoutDate && workoutDate <= toDate;
}

function filterBySearchString(workout, searchString) {
  return workout.name.search(searchString) != -1 || searchString == undefined;
}

function filterWorkouts(currentUser, workouts, filter) {
  let { searchString, exerciseType, workoutVisibility, dateRange } = filter;

  workouts = workouts.filter((workout) =>
    filterBySearchString(workout, searchString)
  );

  workouts = workouts.filter((workout) =>
    filterVisibilityType(currentUser, workout, workoutVisibility)
  );

  workouts = workouts.filter((workout) =>
    filterByExerciseType(workout, exerciseType)
  );

  workouts = workouts.filter((workout) =>
    filterByDateRange(workout, dateRange)
  );

  return workouts;
}

function toggleHideWorkouts(workouts) {
  let workoutAnchors = Array.from(document.querySelectorAll(".workout"));

  const showWorkoutIds = workouts.map((element) => element.id);

  let showWorkoutAnchors = workoutAnchors.filter((element) =>
    showWorkoutIds.includes(parseInt(element.id, 10))
  );
  let hideWorkoutAnchors = workoutAnchors.filter(
    (element) => !showWorkoutIds.includes(parseInt(element.id, 10))
  );

  showWorkoutAnchors.forEach((workoutAnchor) => {
    workoutAnchor.classList.remove("hide");
  });

  hideWorkoutAnchors.forEach((workoutAnchor) => {
    workoutAnchor.classList.add("hide");
  });
}

window.addEventListener("DOMContentLoaded", async () => {
  let createButton = document.querySelector("#btn-create-workout");
  createButton.addEventListener("click", createWorkout);
  let ordering = "-date";

  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has("ordering")) {
    ordering = urlParams.get("ordering");
    if (ordering == "name" || ordering == "owner" || ordering == "date") {
      let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
      aSort.href = `?ordering=-${ordering}`;
    }
  }

  let currentSort = document.querySelector("#current-sort");
  currentSort.innerHTML =
    (ordering.startsWith("-") ? "Descending" : "Ascending") +
    " " +
    ordering.replace("-", "");

  if (ordering.includes("owner")) {
    ordering += "__username";
  }

  let workouts = await fetchWorkouts(ordering);
  renderWorkouts(workouts);
  let favoritedExercises = await sendRequest("GET", `${HOST}/api/favorites/`);
  let data = await favoritedExercises.json();
  fetchExerciseTypes()
    .then((exerciseTypes) => renderExerciseOptions(exerciseTypes, data.results))
    .catch((errorResponse) => {
      console.error(errorResponse);
    });

  let currentUser = await getCurrentUser();

  const fromDateTimeInput = document.querySelector("#workout-from-datetime");
  const toDateTimeInput = document.querySelector("#workout-to-datetime");

  // Set inital datetime for datetime range filter
  fromDateTimeInput.value = FILTER.dateRange[0].toISOString().substr(0, 19);
  toDateTimeInput.value = FILTER.dateRange[1].toISOString().substr(0, 19);

  // Set listeners for datetime range
  fromDateTimeInput.addEventListener("input", () => {
    FILTER.dateRange[0] = fromDateTimeInput.value;
    toggleHideWorkouts(filterWorkouts(currentUser, workouts, FILTER));
  });
  toDateTimeInput.addEventListener("input", () => {
    FILTER.dateRange[1] = toDateTimeInput.value;
    toggleHideWorkouts(filterWorkouts(currentUser, workouts, FILTER));
  });

  // Set listenner for text search
  let searchField = document.querySelector("#input-search-text");
  searchField.addEventListener("input", () => {
    FILTER.searchString = searchField.value;
    toggleHideWorkouts(filterWorkouts(currentUser, workouts, FILTER));
  });

  // Set listener for exercise filter
  const exerciseFilterSelect = document.querySelector(
    "#exercise-filter-select"
  );
  console.debug("element:", exerciseFilterSelect);
  exerciseFilterSelect.addEventListener("change", (event) => {
    console.debug("select event:", event, "event value:", event.target.value);
    FILTER.exerciseType = event.target.value;
    toggleHideWorkouts(filterWorkouts(currentUser, workouts, FILTER));
  });

  // Set listeners for visibilty type tabs
  let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
  for (let i = 0; i < tabEls.length; i++) {
    let tabEl = tabEls[i];
    tabEl.addEventListener("show.bs.tab", function (event) {
      FILTER.workoutVisibility = event.currentTarget.id;
      const filteredWorkouts = filterWorkouts(currentUser, workouts, FILTER);

      toggleHideWorkouts(filteredWorkouts);
    });
  }
});

if (typeof module !== "undefined" && typeof module.exports !== "undefined") {
  module.exports = {
    renderWorkouts,
    renderExerciseOptions,
    toggleHideWorkouts,
    filterWorkouts,
  };
}
