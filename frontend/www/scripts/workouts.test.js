const { fail } = require("assert");
const fs = require("fs");
const path = require("path");
const html = fs.readFileSync(
  path.resolve(__dirname, "../workouts.html"),
  "utf-8"
);

const workoutsModule = require("./workouts.js");

async function login(username, password) {
  let body = { username: username, password: password };

  let response = await sendRequest("POST", `${HOST}/api/token/`, body);
  if (response.ok) {
    let data = await response.json();
    // access and refresh cookies each have a max age of 24 hours
    setCookie("access", data.access, 86400, "/");
    setCookie("refresh", data.refresh, 86400, "/");
  } else {
    let data = await response.json();
    fail("Login failed: " + data);
  }
}

describe("workouts test", () => {
  beforeAll(() => {
    const defaults = window.document.createElement("script");
    defaults.textContent = fs.readFileSync(
      path.resolve(__dirname, "./defaults.js"),
      "utf-8"
    );
    window.document.body.appendChild(defaults);

    const scripts = window.document.createElement("script");
    scripts.textContent = fs.readFileSync(
      path.resolve(__dirname, "./scripts.js"),
      "utf-8"
    );
    window.document.body.appendChild(scripts);

    const api = window.document.createElement("script");
    api.textContent = fs.readFileSync(
      path.resolve(__dirname, "./api.js"),
      "utf-8"
    );
    window.document.body.appendChild(api);

    const workoutsScript = window.document.createElement("script");
    workoutsScript.textContent = fs.readFileSync(
      path.resolve(__dirname, "./workouts.js"),
      "utf-8"
    );
    window.document.body.appendChild(workoutsScript);

    return login("a", "a");
  });

  beforeEach(async () => {
    window.document.documentElement.innerHTML = html.toString();
    let workouts = await fetchWorkouts("date");
    renderWorkouts(workouts);
  });

  it("fetchWorkouts returns workouts as JS object", async () => {
    try {
      let workoutsList = await fetchWorkouts("date");
      expect(workoutsList.length).toBe(2);
    } catch (error) {
      fail(error);
    }
  });

  it("renders workouts", async () => {
    let workoutAnchors = window.document.querySelectorAll(".workout");
    expect(workoutAnchors.length).toBe(2);
  });

  it("Has correct exercises", async () => {
    let workouts = await fetchWorkouts("date");

    let response = await sendRequest("GET", `${HOST}/api/exercises/`);
    let data = await response.json();
    let exercises = data.results;

    expect(workouts[0].exercise_instances[0].exercise).toBe(exercises[0].url);
    expect(workouts[1].exercise_instances[0].exercise).toBe(exercises[1].url);

    /*
            Workouts
            [ { url: 'http://backend:8000/api/exercise-instances/1/',
                id: 1,
                exercise: 'http://backend:8000/api/exercises/1/',
                sets: 2,
                number: 10,
                workout: 'http://backend:8000/api/workouts/1/' } ]
        */

    /*
            Exercise types
             { count: 3,
            next: null,
            previous: null,
            results:
            [ { url: 'http://backend:8000/api/exercises/1/',
                id: 1,
                name: 'Push-up',
                description:
                    'A push-up (or press-up in British English) is a common calisthenics exercise beginning from the prone position.',
                unit: 'reps',
                instances: [Array] },
                { url: 'http://backend:8000/api/exercises/2/',
                id: 2,
                name: 'Crunch',
                description: 'The crunch is one of the most popular abdominal exercises.',
                unit: 'reps',
                instances: [Array] },
                { url: 'http://backend:8000/api/exercises/3/',
                id: 3,
                name: 'Plank',
                description:
                    'The plank is an isometric core strength exercise that involves maintaining a position similar to a push-up for the maximum possible time.',
                unit: 'seconds',
                instances: [] } ] }
        */
  });

  it("Renders exercise type filter options", async () => {
    return fetchExerciseTypes().then((exerciseTypes) => {
      workoutsModule.renderExerciseOptions(exerciseTypes);
      const selectElement = window.document.querySelector(
        "#exercise-filter-select"
      );
      expect(selectElement.children.length).toBe(4);
    });
  });

  it("Filters workouts based on date", async () => {
    const currentUser = await getCurrentUser();
    const workouts = await fetchWorkouts("date");

    let filter = {
      searchString: undefined,
      exerciseType: "any",
      workoutVisibility: undefined,
      dateRange: [
        new Date("2021-02-01T13:00:00"),
        new Date("2021-04-01T13:00:00"),
      ],
    };

    workoutsModule.toggleHideWorkouts(
      workoutsModule.filterWorkouts(currentUser, workouts, filter)
    );

    let workoutAnchors = window.document.querySelectorAll(
      "a.workout:not(.hide)"
    );
    expect(workoutAnchors.length).toBe(2);

    filter = {
      searchString: undefined,
      exerciseType: "any",
      workoutVisibility: undefined,
      dateRange: [
        new Date("2021-03-01T13:00:00"),
        new Date("2021-04-01T13:00:00"),
      ],
    };

    workoutsModule.toggleHideWorkouts(
      workoutsModule.filterWorkouts(currentUser, workouts, filter)
    );

    workoutAnchors = window.document.querySelectorAll("a.workout:not(.hide)");
    expect(workoutAnchors.length).toBe(1);
  });

  it("Filters workouts by search", async () => {
    const currentUser = await getCurrentUser();
    const workouts = await fetchWorkouts("date");

    let filter = {
      searchString: undefined,
      exerciseType: "any",
      workoutVisibility: undefined,
      dateRange: [
        new Date("2019-01-01T13:00:00"),
        new Date("2022-10-01T13:00:00"),
      ],
    };

    workoutsModule.toggleHideWorkouts(
      workoutsModule.filterWorkouts(currentUser, workouts, filter)
    );

    let workoutAnchors = window.document.querySelectorAll(
      "a.workout:not(.hide)"
    );
    expect(workoutAnchors.length).toBe(2);

    filter = {
      searchString: "a",
      exerciseType: "any",
      workoutVisibility: undefined,
      dateRange: [
        new Date("2019-01-01T13:00:00"),
        new Date("2022-10-01T13:00:00"),
      ],
    };

    workoutsModule.toggleHideWorkouts(
      workoutsModule.filterWorkouts(currentUser, workouts, filter)
    );

    workoutAnchors = window.document.querySelectorAll("a.workout:not(.hide)");
    expect(workoutAnchors.length).toBe(1);
  });

  it("Filters workouts by exercise type", async () => {
    const currentUser = await getCurrentUser();
    const workouts = await fetchWorkouts("date");
    const exerciseTypes = await fetchExerciseTypes();

    let filter = {
      searchString: undefined,
      exerciseType: "any",
      workoutVisibility: undefined,
      dateRange: [
        new Date("2019-01-01T13:00:00"),
        new Date("2022-10-01T13:00:00"),
      ],
    };

    workoutsModule.toggleHideWorkouts(
      workoutsModule.filterWorkouts(currentUser, workouts, filter)
    );

    let workoutAnchors = window.document.querySelectorAll(
      "a.workout:not(.hide)"
    );
    expect(workoutAnchors.length).toBe(2);

    filter = {
      searchString: undefined,
      exerciseType: exerciseTypes[0].url,
      workoutVisibility: undefined,
      dateRange: [
        new Date("2019-01-01T13:00:00"),
        new Date("2022-10-01T13:00:00"),
      ],
    };

    workoutsModule.toggleHideWorkouts(
      workoutsModule.filterWorkouts(currentUser, workouts, filter)
    );

    workoutAnchors = window.document.querySelectorAll("a.workout:not(.hide)");
    expect(workoutAnchors.length).toBe(1);
  });
});
