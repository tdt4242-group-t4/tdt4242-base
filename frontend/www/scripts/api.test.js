const fs = require("fs");
const path = require("path");
const api = require("./api.js");

async function login(username, password) {
  let body = { username: username, password: password };

  let response = await sendRequest("POST", `${HOST}/api/token/`, body);
  if (response.ok) {
    let data = await response.json();
    // access and refresh cookies each have a max age of 24 hours
    setCookie("access", data.access, 86400, "/");
    setCookie("refresh", data.refresh, 86400, "/");
  } else {
    let data = await response.json();
    fail("Login failed: " + data);
  }
}

describe("api.js test", () => {
  beforeAll(() => {
    const defaults = window.document.createElement("script");
    defaults.textContent = fs.readFileSync(
      path.resolve(__dirname, "./defaults.js"),
      "utf-8"
    );
    window.document.body.appendChild(defaults);

    const scripts = window.document.createElement("script");
    scripts.textContent = fs.readFileSync(
      path.resolve(__dirname, "./scripts.js"),
      "utf-8"
    );
    window.document.body.appendChild(scripts);

    return login("a", "a");
  });

  it("fetches workouts", async () => {
    return api.fetchWorkouts("date").then((response) => {
      expect(response.length).toBe(2);
    });
  });

  it("fetches exercises", async () => {
    return api.fetchExerciseTypes().then((response) => {
      expect(response.length).toBe(3);
    });
  });
});
