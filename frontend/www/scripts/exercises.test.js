const fs = require("fs");
const path = require("path");
const html = fs.readFileSync(
  path.resolve(__dirname, "../exercises.html"),
  "utf-8"
);

async function login(username, password) {
  let body = { username: username, password: password };

  let response = await sendRequest("POST", `${HOST}/api/token/`, body);
  if (response.ok) {
    let data = await response.json();
    // access and refresh cookies each have a max age of 24 hours
    setCookie("access", data.access, 86400, "/");
    setCookie("refresh", data.refresh, 86400, "/");
  } else {
    let data = await response.json();
    fail("Login failed: " + data);
  }
}

describe("Exercises test", () => {
  beforeAll(() => {
    const defaults = window.document.createElement("script");
    defaults.textContent = fs.readFileSync(
      path.resolve(__dirname, "./defaults.js"),
      "utf-8"
    );
    window.document.body.appendChild(defaults);

    const scripts = window.document.createElement("script");
    scripts.textContent = fs.readFileSync(
      path.resolve(__dirname, "./scripts.js"),
      "utf-8"
    );
    window.document.body.appendChild(scripts);

    const api = window.document.createElement("script");
    api.textContent = fs.readFileSync(
      path.resolve(__dirname, "./api.js"),
      "utf-8"
    );
    window.document.body.appendChild(api);

    const workoutsScript = window.document.createElement("script");
    workoutsScript.textContent = fs.readFileSync(
      path.resolve(__dirname, "./exercises.js"),
      "utf-8"
    );
    window.document.body.appendChild(workoutsScript);

    return login("a", "a");
  });

  beforeEach(async () => {
    window.document.documentElement.innerHTML = html.toString();
    const exerciseTypes = await fetchExerciseTypes();
    const favoritedExercises = await sendRequest(
      "GET",
      `${HOST}/api/favorites/`
    );
    const data = await favoritedExercises.json();
    renderExerciseTypes(exerciseTypes, data.results);
  });

  it("fetchExerciseTypes resolves exercise types", async () => {
    try {
      const exerciseTypes = await fetchExerciseTypes();
      expect(exerciseTypes.length).toBe(3);
    } catch (error) {
      console.debug(error);
      fail(error);
    }
  });

  it("sorts exercises after which exercises that are favorites or not", async () => {
    const exerciseAnchors = window.document.querySelectorAll(".exercise");
    expect(exerciseAnchors[0].querySelector("h5").textContent).toBe("Plank");
    expect(exerciseAnchors[1].querySelector("h5").textContent).toBe("Crunch");
    expect(exerciseAnchors[2].querySelector("h5").textContent).toBe("Push-up");
  });

  it("Renders exercises", async () => {
    const exerciseAnchors = window.document.querySelectorAll(".exercise");
    expect(exerciseAnchors.length).toBe(3);
  });
});
