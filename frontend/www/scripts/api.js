async function fetchExerciseTypes() {
  const url = `${HOST}/api/exercises/`;
  let response = await sendRequest("GET", url);
  if (!response.ok) {
    throw response;
  } else {
    let data = await response.json();

    let exercises = data.results;

    return exercises;
  }
}

async function fetchWorkouts(ordering) {
  const url = `${HOST}/api/workouts/?ordering=${ordering}`;
  let response = await sendRequest("GET", url);

  if (!response.ok) {
    throw response;
  } else {
    let data = await response.json();

    let workouts = data.results;

    return workouts;
  }
}

if (typeof module !== "undefined" && typeof module.exports !== "undefined") {
  module.exports = { fetchExerciseTypes, fetchWorkouts };
}
