let cancelButton;
let saveButton;
let editButton;

function generateForm() {
  let form = document.querySelector("#form-userprofile");

  let formData = new FormData(form);
  let submitForm = new FormData();
  submitForm.append("email", formData.get("email"));
  submitForm.append("phone_number", formData.get("phone_number"));
  submitForm.append("country", formData.get("country"));
  submitForm.append("city", formData.get("city"));
  submitForm.append("street_address", formData.get("street_address"));

  return submitForm;
}

function handleEditUserButtonClick() {
  setReadOnly(false, "#form-userprofile");
  document.querySelector("#inputUsername").readOnly = true;

  cancelButton = document.querySelector("#btn-cancel-profile");
  saveButton = document.querySelector("#btn-ok-profile");
  editButton = document.querySelector("#btn-edit-profile");

  editButton.className += " hide";
  saveButton.className = saveButton.className.replace(" hide", "");
  cancelButton.className = cancelButton.className.replace(" hide", "");

  cancelButton.addEventListener("click", handleCancelDuringUserEdit);
}

function handleCancelDuringUserEdit() {
  window.location.replace("userprofile.html");
}

async function updateUser() {
  let userForm = generateForm();
  let currentUser = await getCurrentUser();
  let response = await sendRequest(
    "PATCH",
    `${HOST}/api/users/${currentUser.id}/`,
    userForm,
    ""
  );
  if (response.ok) {
    let alert = createAlert("Personal information updated!", {}, "success");
    document.body.prepend(alert);
    setTimeout(() => {
      window.location.replace("userprofile.html");
    }, 2000);
  } else {
    let data = await response.json();
    let alert = createAlert("Could not update user!", data);
    document.body.prepend(alert);
  }
}

async function fillForm() {
  let currentUser = await getCurrentUser();
  let form = document.querySelector("#form-userprofile");
  let formData = new FormData(form);

  for (let key of formData.keys()) {
    let selector = `input[name="${key}"], textarea[name="${key}"]`;
    let input = form.querySelector(selector);
    let newVal = currentUser[key];
    input.value = newVal;
  }
}

window.addEventListener("DOMContentLoaded", async () => {
  cancelButton = document.querySelector("#btn-cancel-profile");
  saveButton = document.querySelector("#btn-ok-profile");
  editButton = document.querySelector("#btn-edit-profile");

  await fillForm();

  editButton.addEventListener("click", handleEditUserButtonClick);
  cancelButton.addEventListener("click", handleCancelDuringUserEdit);
  saveButton.addEventListener("click", updateUser);
});
