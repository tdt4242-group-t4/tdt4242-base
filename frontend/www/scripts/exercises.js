async function renderExerciseTypes(exercises, favorites) {
  if (favorites !== undefined) {
    let user_id = JSON.parse(
      atob(
        ("; " + document.cookie)
          .split("; access=")
          .pop()
          .split(";")
          .shift()
          .split(".")[1]
      )
    )["user_id"];
    favorites = favorites.filter(
      (favorite) => parseInt(favorite.user) === parseInt(user_id)
    );
    exercises = exercises
      .sort((a, b) => {
        if (
          favorites.some((favorite) => favorite.exercise === a.id) &&
          !favorites.some((favorite) => favorite.exercise === b.id)
        )
          return 1;
        else if (
          !favorites.some((favorite) => favorite.exercise === a.id) &&
          favorites.some((favorite) => favorite.exercise === b.id)
        )
          return -1;
        else return 0;
      })
      .reverse();
  }
  let container = document.getElementById("div-content");
  let exerciseTemplate = document.querySelector("#template-exercise");
  exercises.forEach((exercise) => {
    const exerciseAnchor = exerciseTemplate.content.firstElementChild.cloneNode(
      true
    );
    exerciseAnchor.href = `exercise.html?id=${exercise.id}`;

    const h5 = exerciseAnchor.querySelector("h5");
    h5.textContent = exercise.name;

    const p = exerciseAnchor.querySelector("p");
    p.textContent = exercise.description;

    const icon = exerciseAnchor.querySelector("i");
    icon.classList.add("fa-star");
    icon.classList.add(
      favorites !== undefined
        ? favorites.some((favorite) => favorite.exercise === exercise.id)
          ? "fas"
          : "far"
        : "far"
    );

    container.appendChild(exerciseAnchor);
  });
}

function createExercise() {
  window.location.replace("exercise.html");
}

// eslint-disable-next-line no-unused-vars
function sortAlphabetically() {
  var parent = document.getElementById("div-content");
  var children = parent.childNodes;
  var childrenAsArray = [];
  children.forEach((child) => childrenAsArray.push(child));

  var sortByFavoritesThenByName = function (a, b) {
    let aIsFave = a.children[0].children[1].classList.contains("fas");
    let bIsFave = b.children[0].children[1].classList.contains("fas");
    if (aIsFave && !bIsFave) return -1;
    else if (!aIsFave && bIsFave) return 1;
    else
      return a.children[0].children[0].innerHTML
        .toLowerCase()
        .localeCompare(b.children[0].children[0].innerHTML.toLowerCase());
  };

  childrenAsArray.sort(sortByFavoritesThenByName);
  for (var i = 0; i < childrenAsArray.length; i++) {
    childrenAsArray[i].parentNode.appendChild(childrenAsArray[i]);
  }
}

window.addEventListener("DOMContentLoaded", async () => {
  let createButton = document.querySelector("#btn-create-exercise");
  createButton.addEventListener("click", createExercise);
  let favoritedExercises = await sendRequest("GET", `${HOST}/api/favorites/`);
  let data = await favoritedExercises.json();
  fetchExerciseTypes()
    .then(async (exercises) => {
      renderExerciseTypes(exercises, data.results);
    })
    .catch(async (response) => {
      data = await response.json();
      let alert = createAlert("Could not retrieve exercise types!", data);
      document.body.prepend(alert);
    });
});
