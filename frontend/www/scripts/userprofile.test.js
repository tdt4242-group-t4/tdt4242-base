const { fail } = require("assert");
const fs = require("fs");
const path = require("path");
const html = fs.readFileSync(
  path.resolve(__dirname, "../userprofile.html"),
  "utf-8"
);

async function login(username, password) {
  let body = { username: username, password: password };

  let response = await sendRequest("POST", `${HOST}/api/token/`, body);
  if (response.ok) {
    let data = await response.json();
    // access and refresh cookies each have a max age of 24 hours
    setCookie("access", data.access, 86400, "/");
    setCookie("refresh", data.refresh, 86400, "/");
  } else {
    let data = await response.json();
    fail("Login failed: " + data);
  }
}

describe("userprofile test", () => {
  beforeAll(() => {
    const defaults = window.document.createElement("script");
    defaults.textContent = fs.readFileSync(
      path.resolve(__dirname, "./defaults.js"),
      "utf-8"
    );
    window.document.body.appendChild(defaults);

    const scripts = window.document.createElement("script");
    scripts.textContent = fs.readFileSync(
      path.resolve(__dirname, "./scripts.js"),
      "utf-8"
    );
    window.document.body.appendChild(scripts);

    const api = window.document.createElement("script");
    api.textContent = fs.readFileSync(
      path.resolve(__dirname, "./api.js"),
      "utf-8"
    );
    window.document.body.appendChild(api);

    const userprofileScript = window.document.createElement("script");
    userprofileScript.textContent = fs.readFileSync(
      path.resolve(__dirname, "./userprofile.js"),
      "utf-8"
    );
    window.document.body.appendChild(userprofileScript);

    return login("a", "a");
  });

  beforeEach(async () => {
    window.document.documentElement.innerHTML = html.toString();
  });

  it("fills form", async () => {
    await fillForm();
    let username = window.document.querySelector("#inputUsername").value;
    expect(username).toBe("a");
  });

  it("allows to edit form", async () => {
    handleEditUserButtonClick();

    expect(window.document.querySelector("#inputEmail").readOnly).toBe(false);
    expect(window.document.querySelector("#inputUsername").readOnly).toBe(true);
  });

  it("changes are saved", async () => {
    //Fix for navigation error ref. https://github.com/jsdom/jsdom/issues/2112
    delete window.location;
    window.location = { replace: jest.fn() };

    window.document.querySelector("#inputEmail").value = "test@test.no";
    await updateUser();
    window.document.documentElement.innerHTML = html.toString();
    await fillForm();
    let email = window.document.querySelector("#inputEmail").value;
    expect(email).toBe("test@test.no");
  });
});
