
let athlete1Token = ""
let coach1Token = "";


describe("Testing workouts according to FR5", () => {
    function addWorkout(data) {
        let standardData = {
            name: 'Workout name',
            date: '2022-12-03T08:30',
            visibility: 'PU',
            notes: 'a note for a workout',
            file: './file.txt',
            exercises: [
                {
                    type: "1",
                    sets: 2,
                    number: 10
                }
            ]
        }

        data = { ...standardData, ...data };

        data.exercises.forEach(exercise => {
            cy.get("#form-workout").get('select[name="type"]').select(exercise.type);
            cy.get("#form-workout").get('input[name="sets"]').type(exercise.sets);
            cy.get("#form-workout").get('input[name="number"]').type(exercise.number);
            cy.get("#form-workout").get("#btn-add-exercise").click();
        });

        cy.get("#form-workout").get('input[name="name"]').invoke("attr", "value", data.name);
        cy.get("#form-workout").get('input[name="date"]').type(data.date);
        cy.get("#form-workout").get('select[name="visibility"]').select(data.visibility);
        cy.get("#form-workout").get('textarea[name="notes"]').invoke("text", data.notes);
        cy.get("#form-workout").get('input[type="file"]').attachFile(data.file);
        cy.get("#form-workout").get("#btn-ok-workout").click();
    }

    function postComment(name, comment) {
        cy.visit("/workouts.html");
        cy.get("h5").contains(name).click();
        cy.wait(1000);
        cy.get("#comment-area").invoke("text", comment);
        cy.get("#post-comment").click();
    }

    function deleteWorkout(name) {
        cy.visit("/workouts.html");
        cy.get("h5").contains(name).click();
        cy.get("#btn-edit-workout").click();
        cy.get("#btn-delete-workout").click();
    }

    function checkWorkoutContent(workoutName) {
        cy.get("#form-workout").get('input[name="name"]').should('have.value', workoutName);
        cy.get("#form-workout").get('input[name="date"]').should('have.value', '2022-12-03T08:30:00');
        cy.get("#form-workout").get('textarea[name="notes"]').should('have.value', 'a note for a workout');
        cy.get("#form-workout").get('#uploaded-files').children().should('have.length', 1);
        cy.get("#comment-list").children().should('have.length.greaterThan', 0);
    }

    before(() => {
        // Athlete data
        cy.visit("/login.html");
        cy.get("form").get('input[name="username"').type("athlete1");
        cy.get("form").get('input[name="password"').type("athlete1");
        cy.get("form").get("#btn-login").click();

        cy.wait(3000);

        cy.getCookie("access").should("exist")
            .then((accessCookie) => {
                athlete1Token = accessCookie.value;

                cy.log("athlete1Token:" + athlete1Token);
                cy.setCookie("access", athlete1Token);

                // Add athlete workouts
                cy.get("#btn-create-workout").click();
                addWorkout({ name: "Athlete private", visibility: "PR" });
                cy.get("#btn-create-workout").click();
                addWorkout({ name: "Athlete coach", visibility: "CO" });
                cy.get("#btn-create-workout").click();
                addWorkout({ name: "Athlete public", visibility: "PU" });

                // Add comments to athlete workouts
                postComment("Athlete private", "Athlete private comment");
                postComment("Athlete coach", "Athlete coach comment");
                postComment("Athlete public", "Athlete public comment");
            });
    });

    before(() => {
        cy.visit("/login.html");
        cy.get("form").get('input[name="username"').type("athlete1");
        cy.get("form").get('input[name="password"').type("athlete1");
        cy.get("form").get("#btn-login").click();

        cy.wait(3000);

        cy.getCookie("access").should("exist")
            .then((accessCookie) => {
                coach1Token = accessCookie.value;
                // Coach data
                cy.setCookie("access", coach1Token);
                cy.log("coach1Value: " + coach1Token);

                // Add coach workouts
                cy.get("#btn-create-workout").click();
                addWorkout({ name: "Coach private", visibility: "PR" });
                cy.get("#btn-create-workout").click();
                addWorkout({ name: "Coach public", visibility: "PU" });

                // Add comments to coach workouts
                postComment("Coach private", "Coach private comment");
                postComment("Coach public", "Coach public comment");
            });
    });

    after(() => {
        cy.setCookie("access", athlete1Token);
        deleteWorkout("Athlete private");
        deleteWorkout("Athlete coach");
        deleteWorkout("Athlete public");

        cy.setCookie("access", coach1Token);
        deleteWorkout("Coach private");
        deleteWorkout("Coach public");
    });
    //For athletes, this means that the workout needs to be either their own
    it("Finds athlete's own workouts", () => {
        cy.setCookie("access", athlete1Token);
        cy.visit("/workouts.html");
        cy.get("h5").contains("Athlete private").click();
        cy.get("#form-workout").children().should("have.length", 16);
        checkWorkoutContent("Athlete private");
    });
    //For athletes, this means that the workout needs to be either their own or public
    it("Athlete find public workouts", () => {
        cy.setCookie("access", athlete1Token);
        cy.visit("/workouts.html");
        cy.get("h5").contains("Coach public").click();
        cy.get("#form-workout").children().should("have.length", 16);
        checkWorkoutContent("Coach public");
    });
    //For coaches, this means that the workout is at least one of their athletes' non-private workouts
    it("Coach finds their athletes' workout", () => {
        cy.setCookie("access", coach1Token);
        cy.visit("/workouts.html");
        cy.get("h5").contains("Athlete coach").click();
        cy.get("#form-workout").children().should("have.length", 16);
        checkWorkoutContent("Athlete coach");
    });
    //OR the workout is public.
    it("Coach finds public workouts", () => {
        cy.setCookie("access", coach1Token);
        cy.visit("/workouts.html");
        cy.get("h5").contains("Athlete public").click();
        cy.get("#form-workout").children().should("have.length", 16);
        checkWorkoutContent("Athlete public");

    });
    it("Coach finds own private workout", () => {
        cy.setCookie("access", coach1Token);
        cy.visit("/workouts.html");
        cy.get("h5").contains("Coach private").click();
        cy.get("#form-workout").children().should("have.length", 16);
        checkWorkoutContent("Coach private");
    });
});