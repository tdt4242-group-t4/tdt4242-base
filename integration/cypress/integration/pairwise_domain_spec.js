var covertable = require('covertable');
var make = covertable.default;
var validUsername = uuidv4();
var validPassword = uuidv4();
var username =  ["", validUsername, "LindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyricLindseySashiOlayinkaLyric "]
var password =  ["", validPassword, "passwordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpassword"]
var password1 = ["", validPassword, "passwordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpasswordpassword"]

var inputs = make([username, password, password1]);
inputs.push([username[1], password[1], password1[1]]);

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

describe('Register', () => {
    it("Loads register page", () => {
      cy.visit('/register.html');
    });
    
    

    inputs.forEach(tuple => {
        var messages = [];
        if(tuple[0] == validUsername && tuple[1] == validPassword && tuple[2] == validPassword) messages.push("A user with that username already exists");
        else if(tuple[0] == username[2]) messages.push("Ensure this field has no more than 150 characters");
        else if(tuple[0] == username[0]) messages.push("This field may not be blank");
        else if(tuple[1] == password[0]) messages.push("This field may not be blank");
        else if(tuple[2] == password1[0]) messages.push("This field may not be blank");
        else if(tuple[1] != tuple[2]) messages.push("Passwords must match!");
        if(tuple[0] == validUsername && tuple[1] == validPassword && tuple[2] == validPassword){
            it("It succeeds in registering with valid credentials", () => {
                /**
                 * A user is technically successfully registered if they get a user with that username already exists
                */
                cy.visit("/register.html");
                register({username: tuple[0], password: tuple[1], password1: tuple[2]}, []);
            })
        }else{
            it("It fails in registering with invalid credentials", () => {
                cy.visit("/register.html");
                register({username: tuple[0], password: tuple[1], password1: tuple[2]}, messages);
            })
        }
    });
  
    function register(inputData, shouldContain) {
      let data = {
        username: "a",
        email: "a@a.no",
        password: "a",
        password1: "a",
        phone_number: "123",
        country: "a",
        city: "a",
        street_address: "a",
      }
  
      data = {...data, ...inputData};
  
      console.debug("data:", data);
  
      for (const key in data) {
        cy.get("form").get('input[name="' + key + '"]').invoke("attr", "value", data[key]);
      }
  
      cy.get("form").get("#btn-create-account").click();
      if(shouldContain.length != 0){
          shouldContain.forEach(message=> {
        cy.contains(message);
        cy.get(".alert-warning").get(".btn-close").click();
      })
      } else{
        cy.location('pathname').should('eq', '/workouts.html');
      }
    }
  })