describe('Boundary testing workouts', () => {
  beforeEach(() => {
    cy.visit("/login.html");
    cy.get("form").get('input[name="username"').type("admin");
    cy.get("form").get('input[name="password"').type("Password");
    cy.get("form").get("#btn-login").click();
    Cypress.Cookies.preserveOnce("access", "refresh");
  });

  it("Add valid workout", () => {
    cy.get("#btn-create-workout").click();
    addWorkout();
  });

  it("Min- boundary name test", () => {
    cy.get("#btn-create-workout").click();
    addWorkout({name: ""});
    cy.location('pathname').should('eq', '/workout.html');
  });
  it("Min boundary name test", () => {
    cy.get("#btn-create-workout").click();
    addWorkout({name: "n"});
    cy.location('pathname').should('eq', '/workouts.html');
  });
  it("Max boundary name test", () => {
    cy.get("#btn-create-workout").click();
    addWorkout({name: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"});
    cy.location('pathname').should('eq', '/workouts.html');
  });
  it("Max+ boundary name test", () => {
    cy.get("#btn-create-workout").click();
    addWorkout({name: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"});
    cy.location('pathname').should('eq', '/workout.html');
  });

  it("Min- boundary notes test", () => {
    cy.get("#btn-create-workout").click();
    addWorkout({notes: ""});
    cy.location('pathname').should('eq', '/workout.html');
  });
  it("Min boundary notes test", () => {
    cy.get("#btn-create-workout").click();
    addWorkout({notes: "n"});
    cy.location('pathname').should('eq', '/workouts.html');
  });
})

function addWorkout(data) {
  let standardData = {
    name: 'Workout name',
    date: '2021-12-03T08:30',
    visibility: 'PU',
    notes: 'a note for a workout',
    file: './file.txt',
    exercises: [
      {
        type: "1",
        sets: 2,
        number: 10
      }
    ]
  }

  data = {...standardData, ...data};
  
  data.exercises.forEach(exercise => {
    cy.get("#form-workout").get('select[name="type"]').select(exercise.type);
    cy.get("#form-workout").get('input[name="sets"]').type(exercise.sets);
    cy.get("#form-workout").get('input[name="number"]').type(exercise.number);
    cy.get("#form-workout").get("#btn-add-exercise").click();
  });
  
  cy.get("#form-workout").get('input[name="name"]').invoke("attr", "value", data.name);
  cy.get("#form-workout").get('input[name="date"]').type(data.date);
  cy.get("#form-workout").get('select[name="visibility"]').select(data.visibility);
  cy.get("#form-workout").get('textarea[name="notes"]').invoke("text", data.notes);
  cy.get("#form-workout").get('input[type="file"]').attachFile(data.file);
  cy.get("#form-workout").get("#btn-ok-workout").click();
}