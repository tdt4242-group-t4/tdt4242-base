describe('Register', () => {
  it("Loads register page", () => {
    cy.visit('/register.html');
  });

  it("Loads api/users", () => {
    cy.visit("/api/users/");
  })

  it("Contains form elements", () => {
    cy.visit("/register.html");

    cy.get("form").children().should("have.length", 9);

    cy.get("form").get('input[name="username"]');
  });

  it("Fails to register existing user", () => {
    cy.viewport(1920, 1080);
    cy.visit("/register.html");

    cy.get("form").get('input[name="username"]').type("a");
    cy.get("form").get('input[name="email"]').type("a@a.no");
    cy.get("form").get('input[name="password"]').type("a");
    cy.get("form").get('input[name="password1"]').type("a");
    cy.get("form").get('input[name="phone_number"]').type("a");
    cy.get("form").get('input[name="country"]').type("a");
    cy.get("form").get('input[name="city"]').type("a");
    cy.get("form").get('input[name="street_address"]').type("a");

    cy.get("form").get("#btn-create-account").click();

    cy.contains("Registration failed!");
  });

  it("Fails to register with invalid email", () => {
    cy.viewport(1920, 1080);
    cy.visit("/register.html");

    register({email: "notEmail.no"}, "Enter a valid email address");
  });

  it("Fails to register with empty username and password fields", () => {
    cy.viewport(1920, 1080);
    cy.visit("/register.html");
    register({username: ""}, "This field may not be blank");

    cy.visit("/register.html");
    register({password: "", password1: ""}, "This field may not be blank");

  });

  // TODO: Fix passwords does not need to match
  // it("Fails to register with passwords not matching", () => {
  //   register({username: uuidv4(), password: "1234", password1: "12345"}, "Passwords must match");
  // });

  function register(inputData, warningMessage) {
    let data = {
      username: "a",
      email: "a@a.no",
      password: "a",
      password1: "a",
      phone_number: "123",
      country: "a",
      city: "a",
      street_address: "a",
    }

    data = {...data, ...inputData};

    cy.log(data);


    for (const key in data) {
      cy.get("form").get('input[name="' + key + '"]').invoke("attr", "value", data[key]);
    }

    cy.get("form").get("#btn-create-account").click();
    cy.contains(warningMessage);
    cy.get(".alert-warning").get(".btn-close").click();
  }

  function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
})