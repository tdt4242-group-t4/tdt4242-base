"""
Contains the model for the comments Django application. Users posts comments(Comment).
"""
from django.db import models
from django.contrib.auth import get_user_model
from workouts.models import Workout


class Comment(models.Model):
    """Django model for a comment left on a workout.

    Attributes:
        owner:       Who posted the comment
        workout:     The workout this comment was left on.
        content:     The content of the comment.
        timestamp:   When the comment was created.
    """

    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="comments"
    )
    workout = models.ForeignKey(
        Workout, on_delete=models.CASCADE, related_name="comments"
    )
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-timestamp"]
