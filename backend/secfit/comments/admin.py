"""Module for registering models from comment app to admin page so that they appear
"""
from django.contrib import admin
from .models import Comment


admin.site.register(Comment)
