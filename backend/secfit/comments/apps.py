"""AppConfig for Comments app
"""
from django.apps import AppConfig


class CommentsConfig(AppConfig):
    """AppConfig for Comments app
    Attributes:
        name (str): The name of the application
    """

    name = "comments"
