"""Serializers for the workouts application
"""
from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField
from comments.models import Comment
from workouts.models import Workout


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Comment. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, owner, workout, content, timestamp

    Attributes:
        owner: The owner of the the comment, represented by their username
        workout:    The associated workout for this instance, represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    workout = HyperlinkedRelatedField(
        queryset=Workout.objects.all(), view_name="workout-detail"
    )

    class Meta:
        model = Comment
        fields = ["url", "id", "owner", "workout", "content", "timestamp"]
