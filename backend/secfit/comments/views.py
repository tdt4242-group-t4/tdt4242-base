"""Contains views for the workouts application. These are mostly class-based views.
"""
from django.db.models import Q
from rest_framework import generics, mixins
from rest_framework import permissions
from rest_framework.filters import OrderingFilter
from secfit.enums import Visibility
from comments.models import Comment
from comments.permissions import IsCommentVisibleToUser
from comments.serializers import CommentSerializer
from workouts.permissions import IsOwner, IsReadOnly


class CommentList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """
    Endpoint for fetching all comments associated with the workout PK
    and creating a new comment
    """

    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [OrderingFilter]
    ordering_fields = ["timestamp"]

    def get(self, request, *args, **kwargs):
        """Method for fetching all comments
        """
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Method for creating a new comment
        """
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        workout_pk = self.kwargs.get("pk")
        qs = Comment.objects.none()

        if workout_pk:
            qs = Comment.objects.filter(workout=workout_pk)
        elif self.request.user:
            qs = Comment.objects.filter(
                Q(workout__visibility=Visibility.PUBLIC)
                | Q(owner=self.request.user)
                | (
                    Q(workout__visibility=Visibility.COACH)
                    & Q(workout__owner__coach=self.request.user)
                )
                | Q(workout__owner=self.request.user)
            ).distinct()

        return qs


class CommentDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):

    """Endpoint for fetching, updating or deleting a specific comment"""

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [
        permissions.IsAuthenticated & IsCommentVisibleToUser & (IsOwner | IsReadOnly)
    ]

    def get(self, request, *args, **kwargs):
        """Method for fetching a specific comment
        """
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Method for editing comment
        """
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Method for deleting comment
        """
        return self.destroy(request, *args, **kwargs)
