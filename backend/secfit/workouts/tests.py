"""
Tests for the workouts application.
"""
from django.test import TestCase, Client
import json
import os
import io

from PIL import Image

# Create your tests here.

class WorkoutUpdateTest(TestCase):
    def generate_photo_file(self):
        file = io.BytesIO()
        image = Image.new("RGBA", size=(100, 100), color=(155, 0, 0))
        image.save(file, "png")
        file.name = "test.png"
        file.seek(0)
        return file

    def setUp(self):
        self.unauthenticatedClient = Client()
        user1 = {
            "username": "dev",
            "password": "devpassword",
            "password1": "devpassword",
            "phone_number": 12312312,
            "email": f"dev@dev.dev",
            "country": "",
            "city": "",
            "street_address": "",
        }

        self.user1 = json.loads(
            self.unauthenticatedClient.post("/api/users/", user1).content
        )
        response = json.loads(
            self.unauthenticatedClient.post(
                "/api/token/",
                json.dumps(
                    {"username": user1["username"], "password": user1["password"]}
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.authenticatedClient1 = Client(
            HTTP_AUTHORIZATION=f"Bearer {response['access']}"
        )

        self.exerciseInstance = json.loads(
            self.authenticatedClient1.post(
                "/api/exercises/",
                json.dumps(
                    {
                        "name": "Sit-up",
                        "description": "Lie down, then sit up using your core",
                        "unit": "Reps",
                    }
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.exerciseInstance2 = json.loads(
            self.authenticatedClient1.post(
                "/api/exercises/",
                json.dumps(
                    {
                        "name": "Push-up",
                        "description": "Push your body upwards using your chest and arms",
                        "unit": "Reps",
                    }
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )

        self.workout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout",
                    "notes": "hello notes",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).content.decode("UTF-8")
        )

        self.workout2 = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "date": "2021-03-05T10:34:00Z",
                    "exercise_instances": "[]",
                    "files": [self.generate_photo_file()],
                },
            ).content.decode("UTF-8")
        )

    def test_can_update(self):
        workoutId = self.workout["id"]
        response = self.authenticatedClient1.put(
                f"/api/workouts/{workoutId}/",
                {
                    "name": "Slow sit-up workout",
                    "notes": "hello notes",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
                )

        self.assertEqual(
            response.status_code,
            200,
        )

        updatedWorkout = json.loads(self.authenticatedClient1.get(f"/api/workouts/{workoutId}/").content.decode("UTF-8"))
        self.assertEqual(
            updatedWorkout["name"],
            "Slow sit-up workout",
        )

    def test_can_update_with_file(self):
        workoutId = self.workout2["id"]
        updatedWorkoutResponse = self.authenticatedClient1.put(
                f"/api/workouts/{workoutId}/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "date": "2021-03-05T10:34:00Z",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ]
                },
                content_type="application/json",
            )
        
        self.assertEqual(updatedWorkoutResponse.status_code, 200)

        response = self.authenticatedClient1.get(f"/api/workouts/{workoutId}/").content.decode("UTF-8")
        updatedWorkoutGet = json.loads(response)

        self.assertEqual(
            self.authenticatedClient1.get(updatedWorkoutGet["files"][0]["url"]).status_code,
            200,
        )

        self.assertEqual(
            updatedWorkoutGet["exercise_instances"][0]["exercise"],
            self.exerciseInstance["url"],
        )

        if os.path.exists("test.png"):
            os.remove("test.png")
        


class WorkoutPermissionTest(TestCase):
    def setUp(self):
        self.unauthenticatedClient = Client()
        user1 = {
            "username": "dev",
            "password": "devpassword",
            "password1": "devpassword",
            "phone_number": 12312312,
            "email": f"dev@dev.dev",
            "country": "",
            "city": "",
            "street_address": "",
        }
        user2 = {
            "username": "user",
            "password": "userpassword",
            "password1": "userpassword",
            "phone_number": 21321321,
            "email": f"user@user.user",
            "country": "",
            "city": "",
            "street_address": "",
        }
        user3 = {
            "username": "idiot",
            "password": "hello",
            "password1": "hello",
            "phone_number": 21321321,
            "email": f"aaa@aaa.aaa",
            "country": "",
            "city": "",
            "street_address": "",
        }
        self.user1 = json.loads(
            self.unauthenticatedClient.post("/api/users/", user1).content
        )
        self.user2 = json.loads(
            self.unauthenticatedClient.post("/api/users/", user2).content
        )
        self.user3 = json.loads(
            self.unauthenticatedClient.post("/api/users/", user3).content
        )
        response = json.loads(
            self.unauthenticatedClient.post(
                "/api/token/",
                json.dumps(
                    {"username": user1["username"], "password": user1["password"]}
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.authenticatedClient1 = Client(
            HTTP_AUTHORIZATION=f"Bearer {response['access']}"
        )
        response = json.loads(
            self.client.post(
                "/api/token/",
                json.dumps(
                    {"username": user2["username"], "password": user2["password"]}
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.authenticatedClient2 = Client(
            HTTP_AUTHORIZATION=f"Bearer {response['access']}"
        )
        response = json.loads(
            self.client.post(
                "/api/token/",
                json.dumps(
                    {"username": user3["username"], "password": user3["password"]}
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.authenticatedClient3 = Client(
            HTTP_AUTHORIZATION=f"Bearer {response['access']}"
        )

        self.exerciseInstance = json.loads(
            self.authenticatedClient1.post(
                "/api/exercises/",
                json.dumps(
                    {
                        "name": "Sit-up",
                        "description": "Lie down, then sit up using your core",
                        "unit": "Reps",
                    }
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.exerciseInstance2 = json.loads(
            self.authenticatedClient1.post(
                "/api/exercises/",
                json.dumps(
                    {
                        "name": "Push-up",
                        "description": "Push your body upwards using your chest and arms",
                        "unit": "Reps",
                    }
                ),
                content_type="application/json",
            ).content.decode("UTF-8")
        )

        self.workout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout",
                    "notes": "hello notes",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).content.decode("UTF-8")
        )

    def test_is_owner(self):
        workoutId = self.workout["id"]
        self.assertEqual(
            self.authenticatedClient1.get(f"/api/workouts/{workoutId}/").status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(f"/api/workouts/{workoutId}/").status_code,
            403,
        )

    def test_is_owner_of_workout(self):
        publicWorkout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).content.decode("UTF-8")
        )

        instanceId = publicWorkout["exercise_instances"][0]["id"]
        self.assertEqual(
            self.authenticatedClient1.get(
                f"/api/exercise-instances/{instanceId}/"
            ).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(
                f"/api/exercise-instances/{instanceId}/"
            ).status_code,
            404,
        )
        self.assertEqual(
            self.authenticatedClient1.post(
                f"/api/exercise-instances/",
                {
                    "exercise": self.exerciseInstance["url"],
                    "workout": publicWorkout["url"],
                    "sets": 1,
                    "number": 1337,
                },
            ).status_code,
            201,
        )
        self.assertEqual(
            self.authenticatedClient1.post(
                f"/api/exercise-instances/",
                {"exercise": self.exerciseInstance["url"], "sets": 1, "number": 1337},
            ).status_code,
            403,
        )
        self.assertEqual(
            self.authenticatedClient2.post(
                f"/api/exercise-instances/",
                {
                    "exercise": self.exerciseInstance["url"],
                    "workout": publicWorkout["url"],
                    "sets": 1,
                    "number": 1337,
                },
            ).status_code,
            403,
        )

    def generate_photo_file(self):
        file = io.BytesIO()
        image = Image.new("RGBA", size=(100, 100), color=(155, 0, 0))
        image.save(file, "png")
        file.name = "test.png"
        file.seek(0)
        return file

    def test_is_workout_public(self):
        publicWorkout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "date": "2021-03-05T10:34:00Z",
                    "exercise_instances": "[]",
                    "files": [self.generate_photo_file()],
                },
            ).content.decode("UTF-8")
        )
        self.assertEqual(
            self.authenticatedClient1.get(publicWorkout["files"][0]["url"]).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(publicWorkout["files"][0]["url"]).status_code,
            200,
        )
        if os.path.exists("test.png"):
            os.remove("test.png")
        privateWorkout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PR",
                    "date": "2021-03-05T10:34:00Z",
                    "exercise_instances": "[]",
                    "files": [self.generate_photo_file()],
                },
            ).content.decode("UTF-8")
        )
        self.assertEqual(
            self.authenticatedClient1.get(
                privateWorkout["files"][0]["url"]
            ).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(
                privateWorkout["files"][0]["url"]
            ).status_code,
            403,
        )
        if os.path.exists("test.png"):
            os.remove("test.png")

    def test_is_public(self):
        publicWorkout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).content.decode("UTF-8")
        )

        self.assertEqual(
            self.authenticatedClient1.get(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            200,
        )
        privateWorkout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V3",
                    "notes": "hello notes",
                    "visibility": "PR",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).content.decode("UTF-8")
        )

        self.assertEqual(
            self.authenticatedClient1.get(
                f"/api/workouts/{privateWorkout['id']}/"
            ).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(
                f"/api/workouts/{privateWorkout['id']}/"
            ).status_code,
            403,
        )

    def test_is_read_only(self):
        publicWorkout = json.loads(
            self.authenticatedClient1.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.assertEqual(
            self.authenticatedClient1.put(
                f"/api/workouts/{publicWorkout['id']}/",
                {
                    "id": publicWorkout["id"],
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient1.post(
                f"/api/workouts/{publicWorkout['id']}/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).status_code,
            405,
        )
        self.assertEqual(
            self.authenticatedClient2.put(
                f"/api/workouts/{publicWorkout['id']}/",
                {
                    "id": publicWorkout["id"],
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "PU",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).status_code,
            403,
        )

        self.assertEqual(
            self.authenticatedClient2.delete(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            403,
        )

        self.assertEqual(
            self.authenticatedClient2.get(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            200,
        )

        self.assertEqual(
            self.authenticatedClient2.options(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            200,
        )

        self.assertEqual(
            self.authenticatedClient2.head(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            200,
        )

    def test_is_coach_and_visible_to_coach(self):
        user2Revised = {
            "username": "user",
            "password": "user",
            "password1": "user",
            "phone_number": 21321321,
            "email": f"user@user.user",
            "country": "",
            "city": "",
            "street_address": "",
            "coach": self.user1["url"],
            "athletes": [],
        }
        self.authenticatedClient2.patch(
            f"/api/users/{self.user2['id']}/",
            json.dumps(user2Revised),
            content_type="application/json",
        )
        publicWorkout = json.loads(
            self.authenticatedClient2.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "CO",
                    "exercise_instances": [
                        {
                            "exercise": self.exerciseInstance["url"],
                            "id": self.exerciseInstance["id"],
                            "sets": "1",
                            "number": "1",
                        }
                    ],
                    "date": "2021-03-05T10:34:00Z",
                },
                content_type="application/json",
            ).content.decode("UTF-8")
        )
        self.assertEqual(
            self.authenticatedClient1.get(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient3.get(
                f"/api/workouts/{publicWorkout['id']}/"
            ).status_code,
            403,
        )

    def test_is_coach_of_workout_and_visible_to_coach(self):
        user3Revised = {"coach": self.user2["url"]}
        self.authenticatedClient3.patch(
            f"/api/users/{self.user3['id']}/",
            json.dumps(user3Revised),
            content_type="application/json",
        )
        publicWorkout = json.loads(
            self.authenticatedClient3.post(
                "/api/workouts/",
                {
                    "name": "Extreme sit-up workout V2",
                    "notes": "hello notes",
                    "visibility": "CO",
                    "files": self.generate_photo_file(),
                    "exercise_instances": "[]",
                    "date": "2021-03-05T10:34:00Z",
                },
            ).content.decode("UTF-8")
        )
        self.assertEqual(
            self.authenticatedClient1.get(publicWorkout["files"][0]["url"]).status_code,
            403,
        )
        self.assertEqual(
            self.authenticatedClient3.get(publicWorkout["files"][0]["url"]).status_code,
            200,
        )
        self.assertEqual(
            self.authenticatedClient2.get(publicWorkout["files"][0]["url"]).status_code,
            200,
        )
        if os.path.exists("test.png"):
            os.remove("test.png")

    def test_favorites(self):
        favorites = json.loads(
            self.authenticatedClient1.get("/api/favorites/").content.decode("UTF-8")
        )["results"]
        self.assertEqual(len(favorites), 0)
        favoriteData = {
            "user": self.user1["id"],
            "exercise": self.exerciseInstance["id"],
        }
        response = self.authenticatedClient1.post("/api/favorites/", favoriteData)
        self.assertEqual(response.status_code, 201)
        newFavorite = json.loads(response.content.decode("UTF-8"))
        self.assertEqual(favoriteData["user"], newFavorite["user"])
        self.assertEqual(favoriteData["exercise"], newFavorite["exercise"])
        favorites = json.loads(
            self.authenticatedClient1.get("/api/favorites/").content.decode("UTF-8")
        )["results"]
        self.assertEqual(len(favorites), 1)

        newFavoriteFromGET = json.loads(
            self.authenticatedClient1.get(
                f"/api/favorites/{newFavorite['id']}/"
            ).content.decode("UTF-8")
        )
        self.assertEqual(newFavoriteFromGET["id"], newFavorite["id"])
        self.assertEqual(newFavoriteFromGET["user"], newFavorite["user"])
        self.assertEqual(newFavoriteFromGET["exercise"], newFavorite["exercise"])
        # Test DELETE /api/favorites/:id
        response = self.authenticatedClient1.delete(
            f"/api/exercises/{newFavorite['id']}/"
        )
        self.assertEqual(response.status_code, 204)
        favoritesAfterDelete = json.loads(
            self.authenticatedClient1.get("/api/favorites/").content.decode("UTF-8")
        )["results"]
        self.assertFalse(len(favorites) == len(favoritesAfterDelete))

    def test_favorites_robustness(self):
        favoriteData = {"user": self.user1["id"]}
        response = self.authenticatedClient1.post("/api/favorites/", favoriteData)
        self.assertEqual(response.status_code, 400)

        favoriteData = {"user": 72, "exercise": self.exerciseInstance["id"]}
        response = self.authenticatedClient1.post("/api/favorites/", favoriteData)
        self.assertEqual(response.status_code, 400)

        favoriteData = {"user": self.user1["id"], "exercise": 123123}
        response = self.authenticatedClient1.post("/api/favorites/", favoriteData)
        self.assertEqual(response.status_code, 400)

        favoriteData = {
            "user": self.user1["username"],
            "exercise": self.exerciseInstance["name"],
        }
        response = self.authenticatedClient1.post("/api/favorites/", favoriteData)
        self.assertEqual(response.status_code, 400)
