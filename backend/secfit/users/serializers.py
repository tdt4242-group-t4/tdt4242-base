"""Module containing the serializers for the different user objects
"""
from rest_framework import serializers
from django.contrib.auth import get_user_model, password_validation
from users.models import Offer, AthleteFile
from django import forms


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    password1 = serializers.CharField(style={"input_type": "password"}, write_only=True)
    """Serializer for a new user, subclass of Djangos HyperlinkedModelSerializer
    """

    class Meta:
        """Overrides the Meta class in the superclass"""

        model = get_user_model()
        fields = [
            "url",
            "id",
            "email",
            "username",
            "password",
            "password1",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
        ]

    def validate_password(self, value):
        """Validates the provided password. Needs to be compliant to the set password policies(checked in the validate_password method)
        and the provided passwords need to match
        """
        data = self.get_initial()

        password = data.get("password")
        password1 = data.get("password1")

        try:
            password_validation.validate_password(password)
        except forms.ValidationError as error:
            raise serializers.ValidationError(error.messages)

        if password != password1:
            raise serializers.ValidationError("Passwords must match!")

        return value

    def create(self, validated_data):
        username = validated_data["username"]
        email = validated_data["email"]
        password = validated_data["password"]
        phone_number = validated_data["phone_number"]
        country = validated_data["country"]
        city = validated_data["city"]
        street_address = validated_data["street_address"]
        user_obj = get_user_model()(
            username=username,
            email=email,
            phone_number=phone_number,
            country=country,
            city=city,
            street_address=street_address,
        )
        user_obj.set_password(password)
        user_obj.save()

        return user_obj


class UserGetSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for fetching an existing User, subclass of Djangos HyperlinkedModelSerializer"""

    class Meta:
        """Overrides the Meta class in the superclass"""

        model = get_user_model()
        fields = [
            "url",
            "id",
            "email",
            "username",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
        ]


class UserPutSerializer(serializers.ModelSerializer):
    """Serializer for editing an existing user, subclass of Djangos HyperlinkedModelSerializer"""

    class Meta:
        """Overrides the Meta class in the superclass"""

        model = get_user_model()
        fields = ["athletes"]

    def update(self, instance, validated_data):
        athletes_data = validated_data["athletes"]
        instance.athletes.set(athletes_data)

        return instance


class AthleteFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for athlete file, subclass of Djangos HyperlinkedModelSerializer"""

    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        """Overrides the Meta class in the superclass"""

        model = AthleteFile
        fields = ["url", "id", "owner", "file", "athlete"]

    def create(self, validated_data):
        return AthleteFile.objects.create(**validated_data)


class OfferSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for athlete offer, subclass of Djangos HyperlinkedModelSerializer"""

    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        """Overrides the Meta class in the superclass"""

        model = Offer
        fields = [
            "url",
            "id",
            "owner",
            "recipient",
            "status",
            "timestamp",
        ]
