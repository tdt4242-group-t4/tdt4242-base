"""Module containing forms for the two User models(new and existing).
"""
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model


class CustomUserCreationForm(UserCreationForm):
    """Subclass of Djangos UserCreationForm"""

    phone_number = forms.CharField(max_length=50)
    country = forms.CharField(max_length=50)
    city = forms.CharField(max_length=50)
    street_address = forms.CharField(max_length=50)

    class Meta(UserCreationForm):
        """Overrides the Meta class in the superclass"""

        model = get_user_model()
        fields = (
            "username",
            "coach",
            "phone_number",
            "country",
            "city",
            "street_address",
        )


class CustomUserChangeForm(UserChangeForm):
    """Subclass of Djangos UserCreationForm"""

    class Meta(UserChangeForm):
        """Overrides the Meta class in the superclass"""

        model = get_user_model()
        fields = ("username", "coach")
