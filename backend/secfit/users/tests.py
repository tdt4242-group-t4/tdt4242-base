from django.test import TestCase
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from users.models import User
from users.serializers import UserSerializer


class UserSerializerTestCase(TestCase):
    def setUp(self):
        self.data = {
            "username": "testuser",
            "email": "testuser@mail.com",
            "password": "password",
            "password1": "password",
            "phone_number": "12345678",
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Address",
        }

    def test_create_user(self):
        serializer = UserSerializer()
        user = serializer.create(validated_data=self.data)
        self.assertEqual(user.username, self.data["username"])
        self.assertEqual(user.email, self.data["email"])
        self.assertEqual(user.phone_number, self.data["phone_number"])
        self.assertEqual(user.country, self.data["country"])
        self.assertEqual(user.city, self.data["city"])
        self.assertEqual(user.street_address, self.data["street_address"])

        stored_user = User.objects.get(username=self.data["username"])
        self.assertEqual(stored_user.username, self.data["username"])
        self.assertEqual(stored_user.email, self.data["email"])
        self.assertEqual(stored_user.phone_number, self.data["phone_number"])
        self.assertEqual(stored_user.country, self.data["country"])
        self.assertEqual(stored_user.city, self.data["city"])
        self.assertEqual(stored_user.street_address, self.data["street_address"])

    def test_validate_password(self):
        serializer = UserSerializer(data={"password": "aa"})
        with self.assertRaises(serializers.ValidationError):
            # "value" is not used, but will raise error if not present
            serializer.validate_password(value="a")

    def test_validate_password_valid(self):
        pw = "avalidpassword"
        serializer = UserSerializer(data={"password": pw, "password1": pw})
        try:
            # "value" is not used, but will raise error if not present
            serializer.validate_password(value="b")
        except serializers.ValidationError:
            self.fail("validate_password() raised ValidationError")
