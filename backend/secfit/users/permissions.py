"""Contains custom DRF permissions classes for the workouts app
"""
from rest_framework import permissions
from django.contrib.auth import get_user_model


class IsCurrentUser(permissions.BasePermission):
    """Checks if the logged in user has object permissions"""

    def has_object_permission(self, request, view, obj):
        return obj == request.user


class IsAthlete(permissions.BasePermission):
    """Checks the permissions for an athlete"""

    def has_permission(self, request, view):
        """Checks if the user has permission."""
        if request.method == "POST":
            if request.data.get("athlete"):
                athlete_id = request.data["athlete"].split("/")[-2]
                return athlete_id == request.user.id
            return False

        return True

    def has_object_permission(self, request, view, obj):
        """Checks if the requesting user has permission to an object"""
        return request.user == obj.athlete


class IsCoach(permissions.BasePermission):
    """Checks the permissions for a coach"""

    def has_permission(self, request, view):
        """Checks if the user has permission."""
        if request.method == "POST":
            if request.data.get("athlete"):
                athlete_id = request.data["athlete"].split("/")[-2]
                athlete = get_user_model().objects.get(pk=athlete_id)
                return athlete.coach == request.user
            return False

        return True

    def has_object_permission(self, request, view, obj):
        """Checks if the requesting user is the coach of the objects athlete,
        if so the coach has object permission
        """
        return request.user == obj.athlete.coach
