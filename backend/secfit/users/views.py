"""Contains views for the users application. These are mostly class-based views.
"""
from rest_framework import mixins, generics

from rest_framework import permissions
from rest_framework.permissions import (
    IsAuthenticatedOrReadOnly,
)
from rest_framework.parsers import MultiPartParser, FormParser
from django.contrib.auth import get_user_model
from django.db.models import Q
from users.serializers import (
    UserSerializer,
    OfferSerializer,
    AthleteFileSerializer,
    UserPutSerializer,
    UserGetSerializer,
)
from users.models import Offer, AthleteFile
from users.permissions import IsCurrentUser, IsAthlete, IsCoach
from workouts.permissions import IsOwner, IsReadOnly
from workouts.mixins import CreateListModelMixin


class UserList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    """Endpoint for getting all users and creating a new user"""

    serializer_class = UserSerializer
    users = []
    admins = []

    def get(self, request, *args, **kwargs):
        """Method for fething all users"""
        self.serializer_class = UserGetSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Method for creating new user"""
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        qs = get_user_model().objects.all()

        if self.request.user:
            # Return the currently logged in user
            status = self.request.query_params.get("user", None)
            if status and status == "current":
                qs = get_user_model().objects.filter(pk=self.request.user.pk)

        return qs


class UserDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Endpoint for fetching a specific user, editing an existing user or deleting a user"""

    lookup_field_options = ["pk", "username"]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    permission_classes = [permissions.IsAuthenticated & (IsCurrentUser | IsReadOnly)]

    def get_object(self):
        for field in self.lookup_field_options:
            if field in self.kwargs:
                self.lookup_field = field
                break

        return super().get_object()

    def get(self, request, *args, **kwargs):
        """Method for fetching a specific user"""
        self.serializer_class = UserGetSerializer
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Method for deleting a user"""
        return self.destroy(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Method for editing a user(with PUT)"""
        self.serializer_class = UserPutSerializer
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        """Method for editing a user(with PATCH)"""
        return self.partial_update(request, *args, **kwargs)


class OfferList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Endpoint for creating and fetching all athlete offers"""

    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        """Method for fetching all offers"""
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Method for creating new offer"""
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        user = self.request.user

        if not user:
            return Offer.objects.none()

        query_set = Offer.objects.filter(Q(owner=user) | Q(recipient=user)).distinct()

        query_params = self.request.query_params

        if not query_params:
            return query_set

        status = query_params.get("status", None)
        if status is not None:
            query_set = query_set.filter(status=status)

        category = query_params.get("category", None)
        if category is not None:
            if category == "sent":
                query_set = query_set.filter(owner=user)
            elif category == "received":
                query_set = query_set.filter(recipient=user)

        return query_set


class OfferDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Endpoint for fetching a specific offer, deleting and editing a offer"""

    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        """Method for fetching a specific offer"""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Method for editing an offer(with PUT)"""
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        """Method for editing an offer(with PATCH)"""
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Method for deleting an offer"""
        return self.destroy(request, *args, **kwargs)


class AthleteFileList(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    CreateListModelMixin,
    generics.GenericAPIView,
):
    """Endpoint for fetching all athlete files and creating a new athlete file"""

    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsCoach)]
    parser_classes = [MultiPartParser, FormParser]

    def get(self, request, *args, **kwargs):
        """Method for fetching all athlete files"""
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Method for creating a new athlete file"""
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        qs = AthleteFile.objects.none()

        if self.request.user:
            qs = AthleteFile.objects.filter(
                Q(athlete=self.request.user) | Q(owner=self.request.user)
            ).distinct()

        return qs


class AthleteFileDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Endpoint for deleting and fetching a specific athlete file"""

    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsOwner)]

    def get(self, request, *args, **kwargs):
        """Method for fetching a specific athlete file"""
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Method for deleting a athlete file"""
        return self.destroy(request, *args, **kwargs)
