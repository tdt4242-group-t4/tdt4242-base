"""Contains the model for the users Django application.
Users(User) can receive offers(Offer) to be a coachs athlete,
and a user can have a file(AthleteFile attached to him/her)
"""
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    """
    Standard Django User model with an added field for a user's coach.
    """

    coach = models.ForeignKey(
        "self", on_delete=models.CASCADE, related_name="athletes", blank=True, null=True
    )
    phone_number = models.TextField(max_length=50, blank=True)
    country = models.TextField(max_length=50, blank=True)
    city = models.TextField(max_length=50, blank=True)
    street_address = models.TextField(max_length=50, blank=True)


def athlete_directory_path(instance, filename):
    """
    Return the path for an athlete's file
    :param instance: Current instance containing an athlete
    :param filename: Name of the file
    :return: Path of file as a string
    """
    return f"users/{instance.athlete.id}/{filename}"


class AthleteFile(models.Model):
    """
    Model for an athlete's file. Contains fields for the athlete for
    whom this file was uploaded, the coach owner, and the file itself.
    """

    athlete = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="coach_files"
    )
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="athlete_files"
    )
    file = models.FileField(upload_to=athlete_directory_path)


class Offer(models.Model):
    """Django model for a coaching offer that one user sends to another.

    Each offer has an owner, a recipient, a status, and a timestamp.

    Attributes:
        owner:       Who sent the offer
        recipient:   Who received the offer
        status:      The current status of the offer (accept, declined, or pending)
        timestamp:   When the offer was sent.
    """

    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="sent_offers"
    )
    recipient = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="received_offers"
    )

    class StatusChoices(models.TextChoices):
        """Enum for possible statuses an offer can have"""

        ACCEPTED = "a", _("Accepted")
        PENDING = "p", _("Pending")
        DECLINED = "d", _("Declined")

    status = models.CharField(
        max_length=8, choices=StatusChoices.choices, default=StatusChoices.PENDING
    )
    timestamp = models.DateTimeField(auto_now_add=True)
