"""AppConfig for Users app
"""
from django.apps import AppConfig


class UsersConfig(AppConfig):
    """AppConfig for Comments app
    Attributes:
    name (str): The name of the application
    """

    name = "users"
