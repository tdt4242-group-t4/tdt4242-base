from django.utils.translation import gettext_lazy as _
from django.db import models


class Visibility(models.TextChoices):
    PUBLIC = "PU", _("Public")
    COACH = "CO", _("Coach")
    PRIVATE = "PR", _("Private")
